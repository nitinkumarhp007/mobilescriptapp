package com.myrx_app.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class PharmacyModel implements Parcelable {

    String pharmacy_id="";
    String pharmacy_name="";
    String address="";
    String phone="";
    String distance="";
    String lat="";
    String lng="";
    String image="";
    public PharmacyModel()
    {}

    protected PharmacyModel(Parcel in) {
        pharmacy_id = in.readString();
        pharmacy_name = in.readString();
        address = in.readString();
        phone = in.readString();
        distance = in.readString();
        lat = in.readString();
        lng = in.readString();
        image = in.readString();
    }

    public static final Creator<PharmacyModel> CREATOR = new Creator<PharmacyModel>() {
        @Override
        public PharmacyModel createFromParcel(Parcel in) {
            return new PharmacyModel(in);
        }

        @Override
        public PharmacyModel[] newArray(int size) {
            return new PharmacyModel[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPharmacy_id() {
        return pharmacy_id;
    }

    public void setPharmacy_id(String pharmacy_id) {
        this.pharmacy_id = pharmacy_id;
    }

    public String getPharmacy_name() {
        return pharmacy_name;
    }

    public void setPharmacy_name(String pharmacy_name) {
        this.pharmacy_name = pharmacy_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pharmacy_id);
        dest.writeString(pharmacy_name);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(distance);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(image);
    }
}
