package com.myrx_app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.myrx_app.Activities.AddReminderActivity;
import com.myrx_app.Activities.LocationsActivity;
import com.myrx_app.Activities.NewPrecriptionActivity;
import com.myrx_app.Activities.RefillStep1Activity;
import com.myrx_app.AlermApp.DashBoardActivity;
import com.myrx_app.R;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeNewFragment extends Fragment {

    Context context;
    Unbinder unbinder;
    @BindView(R.id.refills)
    RelativeLayout refills;
    @BindView(R.id.reminders)
    RelativeLayout reminders;
    @BindView(R.id.locations)
    RelativeLayout locations;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.new_precription)
    RelativeLayout newPrecription;
    private SavePref savePref;

    public HomeNewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        //name_of_pharmacy.setText(savePref.getStringLatest(Parameters.PHARMACY_NAME));

        Glide.with(context).load(savePref.getStringLatest("pharmacy_image")).into(image);


        return view;

    }

    @OnClick({R.id.new_precription, R.id.refills, R.id.reminders, R.id.locations})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_precription:
                startActivity(new Intent(getActivity(), NewPrecriptionActivity.class));
                break;
            case R.id.refills:
                startActivity(new Intent(getActivity(), RefillStep1Activity.class));
                break;
            case R.id.reminders:
              //  startActivity(new Intent(getActivity(), AddReminderActivity.class));
                startActivity(new Intent(getActivity(), DashBoardActivity.class));
                break;
            case R.id.locations:
                startActivity(new Intent(getActivity(), LocationsActivity.class));
                break;
        }
    }
}