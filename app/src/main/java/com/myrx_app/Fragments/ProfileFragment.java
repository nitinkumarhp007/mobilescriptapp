package com.myrx_app.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.myrx_app.Activities.SettingActivity;
import com.myrx_app.R;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    Context context;

    @BindView(R.id.first_name)
    TextView firstName;
    @BindView(R.id.last_name)
    TextView lastName;
    @BindView(R.id.email_address)
    TextView emailAddress;
    @BindView(R.id.mobile_number)
    TextView mobileNumber;
    @BindView(R.id.dob)
    TextView dob;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.insurance_name)
    TextView insuranceName;
    @BindView(R.id.insurance_id)
    TextView insuranceId;
    @BindView(R.id.select_pharmacy)
    TextView selectPharmacy;
    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    private SavePref savePref;
    Unbinder unbinder;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        firstName.setText(savePref.getFirstName());
        lastName.setText(savePref.getLastName());
        emailAddress.setText(savePref.getEmail());
        mobileNumber.setText(savePref.getPhone());
        dob.setText(savePref.getdob());
        address.setText(savePref.getAddress());
        insuranceName.setText(savePref.getInsuranceName());
        insuranceId.setText(savePref.getInsuranceId());
        selectPharmacy.setText(savePref.getStringLatest(Parameters.PHARMACY_NAME));
        Glide.with(context).load(savePref.getImage()).error(R.drawable.place_holder).into(profilePic);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.setting)
    public void onClick() {
        startActivity(new Intent(getActivity(), SettingActivity.class));
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
