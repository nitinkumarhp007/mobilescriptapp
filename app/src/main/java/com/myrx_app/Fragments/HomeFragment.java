package com.myrx_app.Fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.myrx_app.Activities.NewPrecriptionActivity;
import com.myrx_app.Activities.RefillRequestActivity;
import com.myrx_app.Activities.TransferPrecriptionActivity;
import com.myrx_app.R;
import com.myrx_app.Util.BiometricUtils;
import com.myrx_app.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    Context context;
    Unbinder unbinder;
    @BindView(R.id.new_precription)
    RelativeLayout newPrecription;
    @BindView(R.id.refill_request)
    RelativeLayout refillRequest;
    @BindView(R.id.transfer_precription_request)
    RelativeLayout transferPrecriptionRequest;

    private SavePref savePref;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        if (BiometricUtils.isFingerprintAvailable(context) && BiometricUtils.isHardwareSupported(context)) {
            if (savePref.getIsFingerEnable().isEmpty()) {
                IOSDialogFingerprint();
            }
        }


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void IOSDialogFingerprint() {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Would you like to enable finger touch login?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                savePref.setIsFingerEnable("1");
                dialog.dismiss();
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        savePref.setIsFingerEnable("0");
                        dialog.dismiss();
                    }
                }).show();
    }

    @OnClick({R.id.new_precription, R.id.refill_request, R.id.transfer_precription_request})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_precription:
                startActivity(new Intent(getActivity(), NewPrecriptionActivity.class));
                break;
            case R.id.refill_request:
                startActivity(new Intent(getActivity(), RefillRequestActivity.class));
                break;
            case R.id.transfer_precription_request:
                startActivity(new Intent(getActivity(), TransferPrecriptionActivity.class));
                break;
        }
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
}
