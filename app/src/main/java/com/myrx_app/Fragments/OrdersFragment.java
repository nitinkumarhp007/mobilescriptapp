package com.myrx_app.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myrx_app.Activities.LoginActivity;
import com.myrx_app.Adapters.NotificationAdapter;
import com.myrx_app.Adapters.OrderAdapter;
import com.myrx_app.ModelClasses.OrderModel;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrdersFragment extends Fragment {
    Context context;
    Unbinder unbinder;
    @BindView(R.id.orders)
    Button orders;
    @BindView(R.id.pending)
    Button pending;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ArrayList<OrderModel> list;

    public OrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();


        if (ConnectivityReceiver.isConnected()) {
            ORDER_LIST("1", "1");
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.orders, R.id.pending})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.orders:
                orders.setBackground(getResources().getDrawable(R.drawable.drawable_button));
                pending.setBackground(getResources().getDrawable(R.drawable.drawable_button_unselected));
                orders.setTextColor(getResources().getColor(R.color.white));
                pending.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                if (ConnectivityReceiver.isConnected()) {
                    ORDER_LIST("1", "1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.pending:
                pending.setBackground(getResources().getDrawable(R.drawable.drawable_button));
                orders.setBackground(getResources().getDrawable(R.drawable.drawable_button_unselected));
                pending.setTextColor(getResources().getColor(R.color.white));
                orders.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                if (ConnectivityReceiver.isConnected()) {
                    ORDER_LIST("1", "0");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    public void ORDER_LIST(String page, String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllMobileScriptAPIS.ORDER + "/" +
                page + "?limit=100&type=" + type, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray data = jsonmainObject.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                OrderModel orderModel = new OrderModel();

                                orderModel.setTitle(object.getString("title"));
                                orderModel.setId(object.getString("id"));
                                orderModel.setStatus(object.getString("status"));
                                orderModel.setPrice(object.getString("price"));
                                list.add(orderModel);
                            }
                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            recyclerView.setAdapter(new OrderAdapter(context,list));

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
