package com.myrx_app.services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.myrx_app.MainActivity;
import com.myrx_app.R;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;

import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    String message, ads_id, image, notification_code, post_id, medicen_id = "", sender_id, chat_message, timeStamp;
    int notify_count;

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    NotificationManager notificationManager;
    Notification notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());
        message = remoteMessage.getData().get("message");
        // notification_code = remoteMessage.getData().get("notification_code");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        }

        JSONObject object = null;
        try {
            object = new JSONObject(remoteMessage.getData().get("data"));
            notification_code = object.getString("notification_code");
            medicen_id = object.optString("medisense_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (notification_code.equals("1") || notification_code.equals("2")
                || notification_code.equals("3")) {//chat


            publishResultsMessage_Accept(message, notification_code);


            sendNotification(getApplicationContext(), message);

        } else {
            sendNotification(getApplicationContext(), message);
        }


    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("token___", token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;
        if (notification_code.equals("1") || notification_code.equals("2") || notification_code.equals("3")) {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("pharmacy_id", sender_id);
            intent.putExtra("notification_code", notification_code);
            intent.putExtra("message", message);
            intent.putExtra("medicen_id", medicen_id);
        } else {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("notification_code", notification_code);
            intent.putExtra("medicen_id", medicen_id);

        }
        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


       /* Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);*/

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    private void publishResultsMessage_Accept(String message, String notification_code) {
        Intent intent = new Intent(util.NOTIFICATION_MESSAGE_ACCEPT);
        intent.putExtra("message", message);
        intent.putExtra("medicen_id", medicen_id);
        intent.putExtra("notification_code", notification_code);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


}
