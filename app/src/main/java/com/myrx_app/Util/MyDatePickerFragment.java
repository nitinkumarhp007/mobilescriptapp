package com.myrx_app.Util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import com.myrx_app.Activities.EditProfileActivity;
import com.myrx_app.Activities.SignUpActivity;

import java.util.Calendar;

public class MyDatePickerFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), dateSetListener, year, month, day);
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        return mDatePicker;
    }

    private DatePickerDialog.OnDateSetListener dateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                   /* Toast.makeText(getActivity(), "selected date is " + view.getDayOfMonth() +
                            " / " + (view.getMonth() + 1) +
                            " / " + view.getYear(), Toast.LENGTH_SHORT).show();*/

                    /*if (SignUpActivity.dob != null)
                        SignUpActivity.dob.setText(view.getDayOfMonth() + "/" + (view.getMonth() + 1) + "/" + view.getYear());
                    else
                        //EditProfileActivity.dob.setText(view.getDayOfMonth() + "/" + (view.getMonth() + 1) + "/" + view.getYear());
*/
                }
            };
}
