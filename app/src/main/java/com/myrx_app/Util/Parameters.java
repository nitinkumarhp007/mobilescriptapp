package com.myrx_app.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "authorization_key";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String SOCIAL_ID = "soical_id";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String USER_TYPE = "user_type";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String DELIVERY_BY = "delivery_by";

    public static final String INSURANCE_ID = "insurance_id";
    public static final String PHARMACY_ID = "pharmacy_id";
    public static final String PHARMACY_NAME = "pharmacy_name";
    public static final String INSURANCE_NAME = "insurance_name";
    public static final String ADDRESS = "address";
    public static final String DOB = "dob";
    public static final String REQUESTS_MEDISENSE_ID = "requests_medisense_id";
    public static final String MEDISENSE_NAME = "medisense_name";
    public static final String MEDICEN_ID = "medicen_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTIONS ="descriptions" ;
    public static final String IMAGE ="image" ;
    public static final String TOTAL_MEDISENSE ="total_medisense" ;
    public static final String PHARMACY_NUMBER ="pharmacy_number" ;
    public static final String PROFILE = "profile";
    public static final String DELIVERY_TYPE = "delivery_type";
    public static final String STREET_ADDRESS = "street_address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String POSTCODE = "postcode";
    public static final String STREET_ADDRESS2 = "street_address2";
    public static final String USER_EMAIL = "user_email";
    public static final String QUESTION = "question";
    public static final String IMAGES ="images" ;
}















