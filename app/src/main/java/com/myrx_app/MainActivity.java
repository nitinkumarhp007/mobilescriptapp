package com.myrx_app;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.myrx_app.Activities.LoginActivity;
import com.myrx_app.Fragments.HomeFragment;
import com.myrx_app.Fragments.HomeNewFragment;
import com.myrx_app.Fragments.NotificationsFragment;
import com.myrx_app.Fragments.OrdersFragment;
import com.myrx_app.Fragments.ProfileFragment;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;
import com.myrx_app.parser.GetAsyncPUT;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity {
    MainActivity context;
    private SavePref savePref;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    @BindView(R.id.adView)
    AdView mAdView;

    boolean is_from_push = false;
    private String medicen_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = MainActivity.this;
        savePref = new SavePref(context);
        //AddTask();

        Log.e("dataaa", SavePref.getDeviceToken(this, "token"));
        Log.e("dataaa", "ID " + savePref.getID());

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        toolbar.setVisibility(View.VISIBLE);
        title.setText("Home");
        loadFragment(new HomeNewFragment());

        is_from_push = getIntent().getBooleanExtra("is_from_push", false);
        if (is_from_push) {
            String notification_code = getIntent().getStringExtra("notification_code");
            if (!notification_code.equals("10"))
                medicen_id = getIntent().getStringExtra("medicen_id");
            if (notification_code.equals("1")) {
                Dialog_accept(MainActivity.this, getIntent().getStringExtra("message"));
            } else if (notification_code.equals("2") || notification_code.equals("3")) {
                Dialog_reject(MainActivity.this, getIntent().getStringExtra("message"));
            }
        }


    }

    private void AddTask() {
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        mAdView = (AdView) findViewById(R.id.adView);


        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                // Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                // Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            toolbar.setVisibility(View.VISIBLE);
            switch (item.getItemId()) {
                case R.id.navigation_shop:
                    title.setText("Home");
                    loadFragment(new HomeNewFragment());
                    return true;
                case R.id.navigation_gifts:

                    if (savePref.getAuthorization_key().isEmpty()) {
                        toolbar.setVisibility(View.GONE);
                        login_first(MainActivity.this);
                    } else {
                        title.setText("Orders");
                        loadFragment(new OrdersFragment());
                    }


                    return true;
                case R.id.navigation_cart:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        toolbar.setVisibility(View.GONE);
                        login_first(MainActivity.this);
                    } else {
                        title.setText("Notifications");
                        loadFragment(new NotificationsFragment());
                    }

                    return true;
                case R.id.navigation_profile:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        toolbar.setVisibility(View.GONE);
                        login_first(MainActivity.this);
                    } else {
                        title.setText("Profile");
                        toolbar.setVisibility(View.GONE);
                        loadFragment(new ProfileFragment());
                    }
                    return true;
            }
            return false;
        }
    };

    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigation.setSelectedItemId(R.id.navigation_shop);
                    }
                }).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(util.NOTIFICATION_MESSAGE_ACCEPT));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            try {
                medicen_id = bundle.getString("medicen_id");
                if (bundle.getString("notification_code").equals("1")) {
                    Dialog_accept(MainActivity.this, bundle.getString("message"));
                } else if (bundle.getString("notification_code").equals("2") || bundle.getString("notification_code").equals("3")) {
                    Dialog_reject(MainActivity.this, bundle.getString("message"));
                } /*else if (bundle.getString("notification_code").equals("3")) {

                }*/

            } catch (NullPointerException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };


    public void Dialog_accept(Context context, String message) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(message).setPositiveButton("Pickup", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                UPDATE_DELIVERY(medicen_id, "2");
            }
        })
                .setNegativeButton("Delivery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        UPDATE_DELIVERY(medicen_id, "1");
                    }
                }).show();
    }

    public void Dialog_reject(Context context, String message) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
                /*.setNegativeButton("Delivery", null)*/.show();
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    private void UPDATE_DELIVERY(String medicen_id, String delivery_by) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.MEDICEN_ID, medicen_id);
        formBuilder.addFormDataPart(Parameters.DELIVERY_BY, delivery_by);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPUT mAsync = new GetAsyncPUT(context, AllMobileScriptAPIS.UPDATE_DELIVERY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, "Order Updated Successfully");
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
