package com.myrx_app.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.myrx_app.ModelClasses.NotificationModel;
import com.myrx_app.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<NotificationModel> list;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.notification_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.text.setText(list.get(position).getText());

        String timestamp = list.get(position).getCreated(); //timestamp : 1254155422
        long timestampString = Long.parseLong(timestamp);
        String value = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                format(new Date(timestampString * 1000)); //convertion to 16/05/2017 17:33:42

        // holder.time.setText(value);
        Log.e("value", value);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = null;
        try {
            date = (Date) formatter.parse(value);//value:  16/05/2017 17:33:42
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        String date_text = date.toString().substring(0, 10);

        holder.time.setText(date_text);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.time)
        TextView time;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
