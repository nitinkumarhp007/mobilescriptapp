package com.myrx_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.myrx_app.ModelClasses.OrderModel;
import com.myrx_app.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<OrderModel> list;

    public OrderAdapter(Context context, ArrayList<OrderModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.order_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.title.setText(list.get(position).getTitle());
        holder.orderNo.setText("Order No.: " + list.get(position).getId());

        if (list.get(position).getStatus().equals("0")) {
            holder.status.setText("Pending");
            holder.status.setTextColor(context.getResources().getColor(R.color.red));
        } else if (list.get(position).getStatus().equals("1")) {
            holder.status.setText("Accepted");
            holder.status.setTextColor(context.getResources().getColor(R.color.green));
        } else if (list.get(position).getStatus().equals("2")) {
            holder.status.setTextColor(context.getResources().getColor(R.color.red));
            holder.status.setText("Rejected");
        } else if (list.get(position).getStatus().equals("3")) {
            holder.status.setTextColor(context.getResources().getColor(R.color.red));
            holder.status.setText("Retake");
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_no)
        TextView orderNo;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.status)
        TextView status;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
