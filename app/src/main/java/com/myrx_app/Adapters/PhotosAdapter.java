package com.myrx_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.myrx_app.R;
import com.myrx_app.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<String> list;

    public PhotosAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.photo_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context)
                .load(list.get(position)).into(holder.cropImageView);


        holder.crossss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                util.showToast(context, "Photo Removed");
                list.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cropImageView)
        ImageView cropImageView;
        @BindView(R.id.crossss)
        ImageView crossss;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
