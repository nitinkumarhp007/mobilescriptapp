package com.myrx_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.myrx_app.Activities.PharmacyDetailActivity;
import com.myrx_app.ModelClasses.PharmacyModel;
import com.myrx_app.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PharmacyAdapter extends RecyclerView.Adapter<PharmacyAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<PharmacyModel> list;

    public PharmacyAdapter(Context context, ArrayList<PharmacyModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.pharmacy_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PharmacyDetailActivity.class);
                intent.putExtra("data",list.get(position));
                context.startActivity(intent);
            }
        });
        holder.name.setText(list.get(position).getPharmacy_name());
        holder.location.setText(list.get(position).getAddress());
      //  holder.distance.setText(String.valueOf(Double.parseDouble(list.get(position).getDistance()) * 0.621371) + " Miles");
        holder.distance.setText(list.get(position).getDistance() + " Miles");
        holder.phone.setText(list.get(position).getPhone());

       /*

        String timestamp = list.get(position).getCreated(); //timestamp : 1254155422
        long timestampString = Long.parseLong(timestamp);
        String value = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                format(new Date(timestampString * 1000)); //convertion to 16/05/2017 17:33:42

        // holder.time.setText(value);
        Log.e("value", value);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = null;
        try {
            date = (Date) formatter.parse(value);//value:  16/05/2017 17:33:42
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        String date_text = date.toString().substring(0, 10);

        holder.time.setText(date_text);*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.distance)
        TextView distance;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.phone)
        TextView phone;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
