package com.myrx_app.parser;


public class AllMobileScriptAPIS {
    //admin : http://www.myrxapp.com/admin/login
    public static final String BASE_URL = "http://3.19.127.52/apis/v1/";

    public static final String USERLOGIN = BASE_URL + "user/login";
    public static final String USER_SIGNUP = BASE_URL + "user";
    public static final String SOCIAL_LOGIN = BASE_URL + "social_login";
    public static final String VERIFY_OTP = BASE_URL + "user/verifiy";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_password";
    public static final String LOGOUT = BASE_URL + "user/logout";
    public static final String CHANGEPASSWORD = BASE_URL + "change_password";
    public static final String EDIT_PROFILE = BASE_URL + "user/edit";
    public static final String APP_INFO = BASE_URL + "app_info";

    public static final String PRESCRIPTION = BASE_URL + "prescription";
    public static final String REFILL_REQUEST = BASE_URL + "refile-request";
    public static final String TRANSFER_REQUEST = BASE_URL + "transfer-request";
    public static final String PHARMACY = BASE_URL + "pharmacy";
    public static final String NOTIFICATION = BASE_URL + "notification";
    public static final String ORDER = BASE_URL + "order";
    public static final String UPDATE_DELIVERY = BASE_URL + "update-delivery";
    public static final String PHARMACY_DETAILS = BASE_URL + "pharmacy-details";

}
