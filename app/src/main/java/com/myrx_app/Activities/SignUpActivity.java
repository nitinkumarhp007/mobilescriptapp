package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.myrx_app.MainActivity;
import com.myrx_app.ModelClasses.PharmacyModel;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.MyDatePickerFragment;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;
import com.myrx_app.parser.GetAsyncGet;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity {
    SignUpActivity context;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.mobile_number)
    EditText mobileNumber;

    public static EditText dob;

    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.insurance_name)
    EditText insuranceName;
    @BindView(R.id.insurance_id)
    EditText insuranceId;
    @BindView(R.id.select_pharmacy)
    TextView selectPharmacy;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.create)
    Button create;
    @BindView(R.id.country_code)
    TextView countryCode;
    @BindView(R.id.sign_in)
    Button signIn;

    private String pharmacy_id = "";
    private SavePref savePref;
    private ArrayList<PharmacyModel> list;
    private String social_id = "", name = "", email = "";

    private boolean is_social = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        context = SignUpActivity.this;
        is_social = getIntent().getBooleanExtra("is_social", false);
        savePref = new SavePref(context);
        social_id = getIntent().getStringExtra("social_id");
        firstName.setText(getIntent().getStringExtra("name"));
        emailAddress.setText(getIntent().getStringExtra("email"));

        if (is_social) {
            password.setVisibility(View.GONE);
        } else {
            password.setVisibility(View.VISIBLE);

        }


        if (ConnectivityReceiver.isConnected()) {
            //PHARMACY();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        dob = (EditText) findViewById(R.id.dob);
       /* dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // DialogFragment newFragment = new MyDatePickerFragment();
                // newFragment.show(getSupportFragmentManager(), "date picker");
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                new SpinnerDatePickerDialogBuilder()
                        .context(context)
                        .callback(new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        })
                        .spinnerTheme(R.style.NumberPickerStyle)
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(year, month, day)
                        .maxDate(year, month, day)
                        .minDate(1950, 0, 1)
                        .build()
                        .show();
            }
        });
        */
        dob.addTextChangedListener(mDateEntryWatcher);
    }

    private TextWatcher mDateEntryWatcher = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                dob.setText(current);
                dob.setSelection(sel < current.length() ? sel : current.length());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }


    };

    @OnClick({R.id.back, R.id.dob, R.id.select_pharmacy, R.id.country_code, R.id.create, R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.dob:
                break;
            case R.id.select_pharmacy:
                Pharmacy_Listing();
                break;
            case R.id.country_code:
                CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code1, String dialCode, int flagDrawableResID) {
                        picker.dismiss();
                        countryCode.setText(dialCode);
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.create:
                SignupProcess();
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, LoginActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SignupProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (!is_social) {
                if (firstName.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter First Name");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (lastName.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Last Name");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (emailAddress.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Email Address");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                    util.IOSDialog(context, "Please Enter a Vaild Email Address");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (mobileNumber.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Phone Number");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (dob.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Select Date of Birth");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (address.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Address");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (insuranceName.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter insurance Name");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (insuranceId.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Insurance ID");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } /*else if (pharmacy_id.isEmpty()) {
                    util.IOSDialog(context, "Please Select Pharmacy");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } */ else if (password.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Password");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    USER_SIGNUP_API();
                }
            } else {
                if (firstName.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter First Name");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (lastName.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Last Name");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (emailAddress.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Email Address");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                    util.IOSDialog(context, "Please Enter a Vaild Email Address");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (mobileNumber.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Phone Number");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (dob.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Select Date of Birth");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (address.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Address");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (insuranceName.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter insurance Name");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (insuranceId.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Insurance ID");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } /*else if (pharmacy_id.isEmpty()) {
                    util.IOSDialog(context, "Please Select Pharmacy");
                    create.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                }*/ else {
                    socialLoginApi();
                }
            }


        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void socialLoginApi() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.SOCIAL_ID, social_id);
        formBuilder.addFormDataPart(Parameters.SOCIAL_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, firstName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LAST_NAME, lastName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, mobileNumber.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DOB, dob.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ADDRESS, address.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSURANCE_NAME, insuranceName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSURANCE_ID, insuranceId.getText().toString().trim());
        //formBuilder.addFormDataPart(Parameters.PHARMACY_ID, pharmacy_id);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(SignUpActivity.this, "token"));
        formBuilder.addFormDataPart("social_token", SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.SOCIAL_LOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setPhone(body.optString("phone"));
                            savePref.setID(body.getString("id"));
                            savePref.setFirstName(body.getString("first_name"));
                            savePref.setLastName(body.getString("last_name"));
                            savePref.setImage(body.getString("profile"));
                            savePref.setInsuranceName(body.getString("insurance_name"));
                            savePref.setInsuranceId(body.getString("insurance_id"));
                            savePref.setAddress(body.getString("address"));
                            savePref.setdob(body.getString("dob"));
                            savePref.setIs_soical(true);
                            savePref.setPharmacy_name(body.getJSONObject("pharmacy_info").optString("name"));

                           /* Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            util.hideKeyboard(context);
                            util.showToast(context, "Login Sucessfully!!!");*/


                            EDIT_PROFILE_API(body.getString("authorization_key"), true);


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, firstName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LAST_NAME, lastName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, countryCode.getText().toString().trim().substring(1) + mobileNumber.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DOB, dob.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ADDRESS, address.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSURANCE_NAME, insuranceName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSURANCE_ID, insuranceId.getText().toString().trim());
        // formBuilder.addFormDataPart(Parameters.PHARMACY_ID, savePref.getStringLatest(Parameters.PHARMACY_ID));
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(SignUpActivity.this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("data");

                            EDIT_PROFILE_API(body.getString("authorization_key"), false);


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void EDIT_PROFILE_API(String authorization_key, boolean is_social) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PHARMACY_ID, savePref.getStringLatest(Parameters.PHARMACY_ID));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.EDIT_PROFILE, formBody, authorization_key) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            if (!is_social) {
                                Intent intent = new Intent(context, OTPActivity.class);
                                intent.putExtra("authorization_key", authorization_key);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.hideKeyboard(context);

                            } else {
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.showToast(context, "User Registered Sucessfully!!!");
                            }


                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void PHARMACY() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllMobileScriptAPIS.PHARMACY, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("data");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                PharmacyModel pharmacyModel = new PharmacyModel();
                                pharmacyModel.setPharmacy_id(object.getString("id"));
                                pharmacyModel.setPharmacy_name(object.getString("name"));
                                list.add(pharmacyModel);
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Pharmacy_Listing() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose an Pharmacy");

        /*ArrayList to Array Conversion */
        String array[] = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j).getPharmacy_name();

        }


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectPharmacy.setText(list.get(which).getPharmacy_name());
                pharmacy_id = list.get(which).getPharmacy_id();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
