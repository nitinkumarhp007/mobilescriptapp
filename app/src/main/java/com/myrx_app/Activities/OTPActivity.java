package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.myrx_app.MainActivity;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OTPActivity extends AppCompatActivity {
    OTPActivity context;
    private SavePref savePref;
    @BindView(R.id.num1)
    EditText num1;
    @BindView(R.id.num2)
    EditText num2;
    @BindView(R.id.num3)
    EditText num3;
    @BindView(R.id.num4)
    EditText num4;
    @BindView(R.id.resend_otp)
    TextView resendOtp;
    @BindView(R.id.submit_button)
    Button submitButton;
    @BindView(R.id.back_button)
    ImageView backButton;

    String authorization_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        authorization_key = getIntent().getStringExtra("authorization_key");

        context = OTPActivity.this;
        savePref = new SavePref(context);

        num1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num2.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num4.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    util.hideKeyboard(context);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick({R.id.back_button, R.id.submit_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.submit_button:
                OTPTASK();
        }
    }

    private void OTPTASK() {
        if (ConnectivityReceiver.isConnected()) {
            if (num1.getText().toString().isEmpty() || num2.getText().toString().isEmpty() ||
                    num3.getText().toString().isEmpty() || num4.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please enter One Time Password(OTP)");
                submitButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                String entered_otp = num1.getText().toString() + num2.getText().toString() + num3.getText().toString() + num4.getText().toString();
                VERIFY_OTP_API(entered_otp);
            }
        } else
            util.showToast(context, util.internet_Connection_Error);
    }

    private void VERIFY_OTP_API(String otp) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OTP, otp);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(OTPActivity.this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.VERIFY_OTP, formBody, authorization_key) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            util.showToast(context, jsonMainobject.getString("message"));
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setPhone(body.optString("phone"));
                            savePref.setID(body.getString("id"));
                            savePref.setFirstName(body.getString("first_name"));
                            savePref.setLastName(body.getString("last_name"));
                            savePref.setImage(body.getString("profile"));
                            savePref.setIs_soical(false);
                            savePref.setInsuranceName(body.getString("insurance_name"));
                            savePref.setInsuranceId(body.getString("insurance_id"));
                            savePref.setAddress(body.getString("address"));
                            savePref.setdob(body.getString("dob"));
                            savePref.setPharmacy_name(body.getJSONObject("pharmacy_info").optString("name"));


                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.hideKeyboard(context);

                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
