package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsyncGet;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LocationsActivity extends AppCompatActivity {
    LocationsActivity context;
    @BindView(R.id.name_of_pharmacy)
    TextView nameOfPharmacy;
    @BindView(R.id.call)
    TextView call;
    @BindView(R.id.map_)
    TextView map;
    @BindView(R.id.open_time)
    TextView openTime;
    @BindView(R.id.scrollable)
    ScrollView scrollable;
    private SavePref savePref;

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;

    String latitude = "", longitude = "", call_text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        ButterKnife.bind(this);

        context = LocationsActivity.this;
        savePref = new SavePref(context);

        title.setText("Locations");

        if (ConnectivityReceiver.isConnected())
            PHARMACY_DETAILS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }


    private void PHARMACY_DETAILS() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllMobileScriptAPIS.PHARMACY_DETAILS
                + "?id=" + savePref.getStringLatest(Parameters.PHARMACY_ID), formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("data");
                            scrollable.setVisibility(View.VISIBLE);
                            nameOfPharmacy.setText(body.getString("name"));

                            call_text = body.getString("phone");
                            latitude = body.getString("latitude");
                            longitude = body.getString("longitude");


                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @OnClick(R.id.back_button)
    public void onClick() {
        finish();
    }

    @OnClick({R.id.call, R.id.map_})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.call:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", call_text, null));
                startActivity(intent);
                break;
            case R.id.map_:
                loadNavigationView(latitude, longitude);
                break;
        }
    }

    public void loadNavigationView(String lat, String lng) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", Double.parseDouble(lat) + "," + Double.parseDouble(lng));
        String url = builder.build().toString();
        Log.e("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url + "&mode=driving"));
        startActivity(i);
    }
}