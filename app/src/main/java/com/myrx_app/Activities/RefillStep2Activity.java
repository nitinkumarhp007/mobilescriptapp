package com.myrx_app.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.myrx_app.R;
import com.myrx_app.Util.util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RefillStep2Activity extends AppCompatActivity {
    RefillStep2Activity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.rx_number)
    EditText rxNumber;
    @BindView(R.id.name_of_medication)
    EditText nameOfMedication;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.cancel)
    Button cancel;

    String first_name="",last_name="",dob="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refill_step2);
        ButterKnife.bind(this);

        title.setText("Add Medications");

        context=RefillStep2Activity.this;

        first_name=getIntent().getStringExtra("first_name");
        last_name=getIntent().getStringExtra("last_name");
        dob=getIntent().getStringExtra("dob");
    }


    @OnClick({R.id.back_button, R.id.next, R.id.cancel})
    public void onClick(View view) {
        util.hideKeyboard(this);
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.next:
                if (rxNumber.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Rx Number");
                    next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    Intent intent = new Intent(this, RefillStep3Activity.class);
                    intent.putExtra("first_name", first_name);
                    intent.putExtra("last_name", last_name);
                    intent.putExtra("dob", dob);
                    intent.putExtra("rx_number", rxNumber.getText().toString());
                    intent.putExtra("medication_name", nameOfMedication.getText().toString());
                    startActivity(intent);
                }
                break;
            case R.id.cancel:
                finish();
                break;
        }
    }
}