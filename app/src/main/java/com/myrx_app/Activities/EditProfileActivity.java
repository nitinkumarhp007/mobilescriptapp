package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.myrx_app.ModelClasses.PharmacyModel;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;
import com.myrx_app.parser.GetAsyncGet;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditProfileActivity extends AppCompatActivity {
    EditProfileActivity context;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.email_address)
    TextView emailAddress;
    @BindView(R.id.mobile_number)
    TextView mobileNumber;

    public TextView dob;

    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.insurance_name)
    EditText insuranceName;
    @BindView(R.id.update)
    Button update;
    @BindView(R.id.insurance_id)
    EditText insuranceId;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.select_pharmacy)
    TextView selectPharmacy;
    private String pharmacy_id = "";
    private SavePref savePref;
    private ArrayList<PharmacyModel> list;

    private int MEDIA_TYPE_GALLERY = 1;
    private int MEDIA_TYPE_CAPTURE = 2;
    private String selectedimage = "";
    Uri fileUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        context = EditProfileActivity.this;
        savePref = new SavePref(context);

        dob = (TextView) findViewById(R.id.dob);


        title.setText("Edit Profile");
        setdata();

        if (ConnectivityReceiver.isConnected()) {
            PHARMACY();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void setdata() {
        firstName.setText(savePref.getFirstName());
        lastName.setText(savePref.getLastName());
        emailAddress.setText(savePref.getEmail());
        mobileNumber.setText(savePref.getPhone());
        dob.setText(savePref.getdob());
        address.setText(savePref.getAddress());
        insuranceName.setText(savePref.getInsuranceName());
        insuranceId.setText(savePref.getInsuranceId());
        selectPharmacy.setText(savePref.getStringLatest(Parameters.PHARMACY_NAME));
        Glide.with(context).load(savePref.getImage()).error(R.drawable.place_holder).into(profilePic);
    }

    private void PHARMACY() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllMobileScriptAPIS.PHARMACY, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("data");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                PharmacyModel pharmacyModel = new PharmacyModel();
                                pharmacyModel.setPharmacy_id(object.getString("id"));
                                pharmacyModel.setPharmacy_name(object.getString("name"));
                                list.add(pharmacyModel);
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.select_pharmacy, R.id.back_button, R.id.update, R.id.profile_pic, R.id.dob})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.select_pharmacy:
                //Pharmacy_Listing();
                break;
            case R.id.profile_pic:
                options();
                break;
            case R.id.update:
                EDIT_PROFILEProcess();
                break;
            case R.id.dob:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);


                new SpinnerDatePickerDialogBuilder()
                        .context(context)
                        .callback(new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        })
                        .spinnerTheme(R.style.NumberPickerStyle)
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(year, month, day)
                        .maxDate(year, month, day)
                        .minDate(1950, 0, 1)
                        .build()
                        .show();
                break;
        }
    }

    private void Pharmacy_Listing() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose an Pharmacy");

        /*ArrayList to Array Conversion */
        String array[] = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j).getPharmacy_name();

        }


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectPharmacy.setText(list.get(which).getPharmacy_name());
                pharmacy_id = list.get(which).getPharmacy_id();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void EDIT_PROFILEProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (firstName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter First Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (lastName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Last Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (dob.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Select Date of Birth");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (address.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (insuranceName.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter insurance Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (insuranceId.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Insurance ID");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                EDIT_PROFILE_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void EDIT_PROFILE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, firstName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LAST_NAME, lastName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, mobileNumber.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DOB, dob.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ADDRESS, address.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSURANCE_NAME, insuranceName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSURANCE_ID, insuranceId.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHARMACY_ID, pharmacy_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setEmail(body.getString("email"));
                            savePref.setPhone(body.optString("phone"));
                            savePref.setFirstName(body.getString("first_name"));
                            savePref.setLastName(body.getString("last_name"));
                            savePref.setImage(body.getString("profile"));
                            savePref.setInsuranceName(body.getString("insurance_name"));
                            savePref.setInsuranceId(body.getString("insurance_id"));
                            savePref.setAddress(body.getString("address"));
                            savePref.setdob(body.getString("dob"));
                            savePref.setPharmacy_name(body.getJSONObject("pharmacy_info").optString("name"));
                            finish();
                            util.showToast(context,jsonMainobject.getString("message"));

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void options() {
        final Item[] items = {
                new Item(getResources().getString(R.string.Camera), android.R.drawable.ic_menu_camera),
                new Item(getResources().getString(R.string.Choose_from_Gallery), android.R.drawable.ic_menu_gallery),
                new Item(getResources().getString(R.string.Cancel), android.R.drawable.ic_notification_clear_all),
        };
        ListAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new AlertDialog.Builder(this)
                .setTitle("Source")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            captureImage();

                            dialog.dismiss();
                        } else if (item == 1) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, MEDIA_TYPE_GALLERY);
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                        }
                    }
                }).show();
    }


    public static class Item {
        public final String text;
        public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private void captureImage() {
        //useful in android naught 7.0
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, MEDIA_TYPE_CAPTURE);
        } else {
            //Toast.makeText(mainActivity, "No Camera Found", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_TYPE_GALLERY) {
            if (resultCode == RESULT_OK) {
                selectedimage = getAbsolutePath(context, data.getData());

                Glide.with(this).load(selectedimage).into(profilePic);

            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        } else if (requestCode == MEDIA_TYPE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                bitmap(fileUri);
                selectedimage = util.getPath(this, fileUri);
                Glide.with(this).load(selectedimage).into(profilePic);
            } else if (resultCode == RESULT_CANCELED) {
                /*Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();*/
            }
        }
    }

    private void bitmap(Uri resultUri) {
        Bitmap bitmap = null;
        try {
            bitmap = (MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

}
