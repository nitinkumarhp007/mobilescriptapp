package com.myrx_app.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.myrx_app.R;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RefillStep1Activity extends AppCompatActivity {
    RefillStep1Activity context;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.dob)
    EditText dob;
    @BindView(R.id.name_of_pharmacy)
    TextView nameOfPharmacy;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.cancel)
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refill_step1);
        ButterKnife.bind(this);

        context=RefillStep1Activity.this;
        savePref=new SavePref(context);

        nameOfPharmacy.setText(savePref.getStringLatest(Parameters.PHARMACY_NAME));



        title.setText("Patient Details");

        dob.addTextChangedListener(mDateEntryWatcher);
    }

    private TextWatcher mDateEntryWatcher = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8){
                    clean = clean + ddmmyyyy.substring(clean.length());
                }else{
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day  = Integer.parseInt(clean.substring(0,2));
                    int mon  = Integer.parseInt(clean.substring(2,4));
                    int year = Integer.parseInt(clean.substring(4,8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon-1);
                    year = (year<1900)?1900:(year>2100)?2100:year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;
                    clean = String.format("%02d%02d%02d",day, mon, year);
                }

                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                dob.setText(current);
                dob.setSelection(sel < current.length() ? sel : current.length());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }


    };

    @OnClick({R.id.back_button, R.id.next, R.id.cancel})
    public void onClick(View view) {
        util.hideKeyboard(this);
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.next:
                if (firstName.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Enter First Name");
                    next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (lastName.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Last Name");
                    next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (dob.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Date of Birth");
                    next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    Intent intent = new Intent(this, RefillStep2Activity.class);
                    intent.putExtra("first_name", firstName.getText().toString());
                    intent.putExtra("last_name", lastName.getText().toString());
                    intent.putExtra("dob", dob.getText().toString());
                    startActivity(intent);
                }

                break;
            case R.id.cancel:
                finish();
                break;
        }
    }
}