package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.myrx_app.MainActivity;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RefillStep3Activity extends AppCompatActivity {
    RefillStep3Activity context;
    private SavePref savePref;

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.pickup)
    Button pickup;
    @BindView(R.id.delivery)
    Button delivery;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.street_address)
    EditText streetAddress;
    @BindView(R.id.street_address_2)
    EditText streetAddress2;
    @BindView(R.id.city)
    EditText city;
    @BindView(R.id.state)
    EditText state;
    @BindView(R.id.zip)
    EditText zip;
    @BindView(R.id.delivery_layout)
    LinearLayout deliveryLayout;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.comments)
    EditText comments;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.cancel)
    Button cancel;

    String first_name = "", last_name = "", dob = "", rx_number = "", medication_name = "";

    String delivery_type = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refill_step3);
        ButterKnife.bind(this);

        context = RefillStep3Activity.this;
        savePref = new SavePref(context);

        first_name = getIntent().getStringExtra("first_name");
        last_name = getIntent().getStringExtra("last_name");
        dob = getIntent().getStringExtra("dob");
        rx_number = getIntent().getStringExtra("rx_number");
        medication_name = getIntent().getStringExtra("medication_name");

        deliveryLayout.setVisibility(View.GONE);
        title.setText("Delivery Options");
    }

    @OnClick({R.id.back_button, R.id.pickup, R.id.delivery, R.id.next, R.id.cancel})
    public void onClick(View view) {
        util.hideKeyboard(this);
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.pickup:
                delivery_type = "0";
                pickup.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                delivery.setTextColor(getResources().getColor(R.color.black));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                deliveryLayout.setVisibility(View.GONE);
                break;
            case R.id.delivery:
                delivery_type = "1";
                deliveryLayout.setVisibility(View.VISIBLE);
                pickup.setTextColor(getResources().getColor(R.color.black));
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                delivery.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                break;
            case R.id.next:
                if (savePref.getAuthorization_key().equals("")) {
                    login_first(context);
                } else {
                    if (ConnectivityReceiver.isConnected()) {
                        if (delivery_type.equals("0")) {
                            REFILL_REQUEST_API();
                        } else {
                            if (streetAddress.getText().toString().isEmpty()) {
                                util.IOSDialog(context, "Please Enter Street Address");
                                next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                            } else if (city.getText().toString().isEmpty()) {
                                util.IOSDialog(context, "Please Enter City");
                                next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                            } else if (state.getText().toString().isEmpty()) {
                                util.IOSDialog(context, "Please Enter State");
                                next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                            } else if (zip.getText().toString().isEmpty()) {
                                util.IOSDialog(context, "Please Enter Zip Code");
                                next.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                            } else {
                                REFILL_REQUEST_API();
                            }
                        }
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }

                break;
            case R.id.cancel:
                finish();
                break;
        }
    }

    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }


    private void REFILL_REQUEST_API() {
        if (medication_name.equals(""))
            medication_name = "test";
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUESTS_MEDISENSE_ID, rx_number);
        formBuilder.addFormDataPart(Parameters.MEDISENSE_NAME, medication_name);
        formBuilder.addFormDataPart(Parameters.DELIVERY_TYPE, delivery_type);
        formBuilder.addFormDataPart(Parameters.PHARMACY_ID, savePref.getStringLatest(Parameters.PHARMACY_ID));
        formBuilder.addFormDataPart(Parameters.STREET_ADDRESS, streetAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CITY, city.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.STATE, state.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.POSTCODE, zip.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.STREET_ADDRESS2, streetAddress2.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.USER_EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.QUESTION, comments.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.REFILL_REQUEST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setMessage("Data Submiited Succesfully").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


}