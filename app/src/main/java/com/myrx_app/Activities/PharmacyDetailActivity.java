package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.myrx_app.MainActivity;
import com.myrx_app.ModelClasses.PharmacyModel;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PharmacyDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    PharmacyDetailActivity context;
    private SavePref savePref;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.submit)
    Button submit;

    PharmacyModel pharmacyModel;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.distance)
    TextView distance;
    private float currentZoom = 15;
    private GoogleMap mMap;

    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy_detail);
        ButterKnife.bind(this);

        context = PharmacyDetailActivity.this;
        savePref = new SavePref(context);

        pharmacyModel = getIntent().getParcelableExtra("data");

        name.setText(pharmacyModel.getPharmacy_name());
        address.setText(pharmacyModel.getAddress());
        distance.setText(pharmacyModel.getDistance() + " Miles");
        phone.setText(pharmacyModel.getPhone());

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.proselectlocationmapid);
        mapFragment.getMapAsync(this);
    }


    private void Alert() {
        new IOSDialog.Builder(context)
                .setTitle("Confirm " + pharmacyModel.getPharmacy_name())
                .setMessage("Confirm to establish a permanent,secure connction to your pharmacy.To change pharmacies in future,reinstall the app & start over.")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ConnectivityReceiver.isConnected()) {

                            util.showToast(context, "Pharmacy Selected Successfully!");



                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                            savePref.setStringLatest(Parameters.PHARMACY_NAME,pharmacyModel.getPharmacy_name());
                            savePref.setStringLatest("pharmacy_image",pharmacyModel.getImage());
                            savePref.setStringLatest(Parameters.PHARMACY_ID,pharmacyModel.getPharmacy_id());

                            //EDIT_PROFILE_API();
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }

                    }
                })
                .setNegativeButton("Undo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @OnClick({R.id.back, R.id.submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.submit:
                Alert();
                break;
        }
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (pharmacyModel.getLat() != null) {
            if (!pharmacyModel.getLat().isEmpty()) {
                LatLng latLng1 = new LatLng(Double.parseDouble(pharmacyModel.getLat()),
                        Double.parseDouble(pharmacyModel.getLng()));

                marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng1)
                        .draggable(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_3x)));
                marker.setTag(999);


                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        latLng1, currentZoom);
                Log.e("ZOOM__", String.valueOf(currentZoom));
                mMap.animateCamera(location, 2100, null);
            }
        }
    }
}