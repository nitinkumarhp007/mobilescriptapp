package com.myrx_app.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.biometrics.BiometricPrompt;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.myrx_app.MainActivity;
import com.myrx_app.R;
import com.myrx_app.Util.BiometricUtils;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.FingerprintHelper;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.Arrays;

import javax.crypto.Cipher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity {
    LoginActivity context;
    private SavePref savePref;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.facebook)
    LoginButton Facebookbutton;
    @BindView(R.id.google)
    ImageView google;


    private FingerprintHelper fingerprintHelper;
    private FingerprintManager fingerprintManager;

    CallbackManager callbackManager;
    String fb_occupation = "", fb_username = "", fb_phone = "", fb_email = "", fb_image = "", social_id = "",
            fb_gender = "", fb_dob = "", fb_firstname = "", fb_lastname = "", socail_id = "";
    ProgressDialog mDialog;
    private CancellationSignal cancellationSignal;
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions googleSignInOptions;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        context = LoginActivity.this;
        savePref = new SavePref(context);
        mDialog = new ProgressDialog(context);
        callbackManager = CallbackManager.Factory.create();
        //googleplus login :)
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();


        Facebookbutton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

        if (ConnectivityReceiver.isConnected()) {
            facebookLogin();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        if (BiometricUtils.isSdkVersionSupported()) {
            checkBiometricSupport();
        }


        if (!savePref.getAuthorization_key().isEmpty() && savePref.getIsFingerEnable().equals("1")) {
            if (BiometricUtils.isSdkVersionSupported()) {
                authenticateUser();
            }

        }
    }

    private Boolean checkBiometricSupport() {

        KeyguardManager keyguardManager =
                (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        PackageManager packageManager = this.getPackageManager();

        if (!keyguardManager.isKeyguardSecure()) {
            notifyUser("Lock screen security not enabled in Settings");
            return false;
        }

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.USE_BIOMETRIC) !=
                PackageManager.PERMISSION_GRANTED) {

            notifyUser("Fingerprint authentication permission not enabled");
            return false;
        }

        if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            return true;
        }

        return true;
    }

    private void notifyUser(String message) {
        Toast.makeText(this,
                message,
                Toast.LENGTH_LONG).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private BiometricPrompt.AuthenticationCallback getAuthenticationCallback() {

        return new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              CharSequence errString) {
                notifyUser("Authentication error: " + errString);

                super.onAuthenticationError(errorCode, errString);
            }

            @Override
            public void onAuthenticationHelp(int helpCode,
                                             CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }

            @Override
            public void onAuthenticationSucceeded(
                    BiometricPrompt.AuthenticationResult result) {
                notifyUser("Authentication Succeeded");
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                super.onAuthenticationSucceeded(result);
            }
        };
    }

    private CancellationSignal getCancellationSignal() {

        cancellationSignal = new CancellationSignal();
        cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
            @Override
            public void onCancel() {
                notifyUser("Cancelled via signal");
            }
        });
        return cancellationSignal;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public void authenticateUser() {
        BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setSubtitle("Authentication is required to continue")
                .setDescription("This app uses biometric authentication to protect your data.")
                .setNegativeButton("Cancel", this.getMainExecutor(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //notifyUser("Authentication cancelled");
                            }
                        })
                .build();

        biometricPrompt.authenticate(getCancellationSignal(), getMainExecutor(),
                getAuthenticationCallback());
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @OnClick({R.id.forgot_password, R.id.google, R.id.sign_in, R.id.sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.google:
                signInWithGplus();
                break;
            case R.id.sign_in:
                SigninProcess();
                break;
            case R.id.sign_up:
                startActivity(new Intent(this, SignUpActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        //Log.e("device_token__", SavePref.getDeviceToken(LoginActivity.this, "token"));
        //String s = SavePref.getDeviceToken(LoginActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(LoginActivity.this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");

                            if (body.getString("status").equals("1")) {
                                savePref.setAuthorization_key(body.getString("authorization_key"));
                                savePref.setEmail(body.getString("email"));
                                savePref.setPhone(body.optString("phone"));
                                savePref.setID(body.getString("id"));
                                savePref.setFirstName(body.getString("first_name"));
                                savePref.setLastName(body.getString("last_name"));
                                savePref.setImage(body.getString("profile"));
                                savePref.setIs_soical(false);
                                savePref.setInsuranceName(body.getString("insurance_name"));
                                savePref.setInsuranceId(body.getString("insurance_id"));
                                savePref.setAddress(body.getString("address"));
                                savePref.setdob(body.getString("dob"));
                                savePref.setPharmacy_name(body.getJSONObject("pharmacy_info").optString("name"));

                                EDIT_PROFILE_API();




                            } else {
                                Toast.makeText(LoginActivity.this, "Kindly verify your account first", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, OTPActivity.class);
                                intent.putExtra("authorization_key", body.getString("authorization_key"));
                                startActivity(intent);
                            }
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void EDIT_PROFILE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PHARMACY_ID, savePref.getStringLatest(Parameters.PHARMACY_ID));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void facebookLogin() {
        Facebookbutton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday",
                "user_friends", "user_photos"));
        Facebookbutton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mDialog.setMessage("please wait.....");
                mDialog.show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                try {
                                    fb_email = object.getString("email");
                                    savePref.setEmail(fb_email);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    social_id = object.getString("id");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_image = "http://graph.facebook.com/" + socail_id + "/picture?type=large";
                                    savePref.setImage(fb_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                   /* fb_cover_image = "https://graph.facebook.com/"
                                            + socail_id + "?fields=cover&access_token=" + getCurrentAccessToken().getToken();*/

                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    JSONObject sourceObj = jsonObject.getJSONObject("cover");
                                    String source = sourceObj.getString("source");
                                    // savePref.setCover_pic(source);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_username = object.getString("name");
                                    savePref.setName(fb_username);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_firstname = object.getString("first_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_lastname = object.getString("last_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_gender = object.getString("gender");
                                    fb_gender = fb_gender.substring(0, 1).toUpperCase()
                                            + fb_gender.substring(1, fb_gender.length());
                                    savePref.setGender(fb_gender);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_dob = object.getString("birthday");
                                    //savePref.setDOB(fb_dob);

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                                socialLoginApi(fb_firstname + " " + fb_lastname, "1", fb_email);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
                mDialog.dismiss();
            }

            @Override
            public void onCancel() {
                mDialog.dismiss();
            }


            @Override
            public void onError(FacebookException error) {
                mDialog.dismiss();
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoginManager.getInstance().logOut();
    }

    private void socialLoginApi(final String NAME, final String SOCIAL_TYPE, final String EMAIL) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.SOCIAL_ID, social_id);
        formBuilder.addFormDataPart(Parameters.SOCIAL_TYPE, SOCIAL_TYPE);
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, NAME);
        formBuilder.addFormDataPart(Parameters.EMAIL, EMAIL);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        formBuilder.addFormDataPart("social_token", SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.SOCIAL_LOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            if (!body.getString("insurance_id").isEmpty()) {
                                savePref.setAuthorization_key(body.getString("authorization_key"));
                                savePref.setEmail(body.getString("email"));
                                savePref.setPhone(body.optString("phone"));
                                savePref.setID(body.getString("id"));
                                savePref.setFirstName(body.getString("first_name"));
                                savePref.setLastName(body.getString("last_name"));
                                savePref.setImage(body.getString("profile"));
                                savePref.setInsuranceName(body.getString("insurance_name"));
                                savePref.setInsuranceId(body.getString("insurance_id"));
                                savePref.setAddress(body.getString("address"));
                                savePref.setdob(body.getString("dob"));
                                savePref.setIs_soical(true);
                                savePref.setPharmacy_name(body.getJSONObject("pharmacy_info").optString("name"));

                                EDIT_PROFILE_API();

                                /*Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                util.hideKeyboard(context);
                                util.showToast(context, "Login Sucessfully!!!");*/
                            } else {
                                Intent intent = new Intent(context, SignUpActivity.class);
                                intent.putExtra("social_id", social_id);
                                intent.putExtra("name", NAME);
                                intent.putExtra("email", EMAIL);
                                intent.putExtra("is_social", true);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            }


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    //Google Plus Login
    //This function will option signing intent
    private void signInWithGplus() {
        try {
            mDialog.setMessage("please wait.....");
            mDialog.show();
        } catch (Exception e) {

        }

        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //For both google & fb login
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //for fb login
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //for google+ login
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);

        }
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {

        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            social_id = acct.getId();
            fb_username = acct.getGivenName();
            fb_email = acct.getEmail();
            Uri personPhoto = acct.getPhotoUrl();
            String username = acct.getDisplayName();
            socialLoginApi(fb_username, "2", fb_email);
            mDialog.setMessage("please wait.....");
            mDialog.show();
        } else {
            //If login fails
            mDialog.show();
            Toast.makeText(this, "" + result.getStatus().toString(), Toast.LENGTH_LONG).show();
        }

    }

}
