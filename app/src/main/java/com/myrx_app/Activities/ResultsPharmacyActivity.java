package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myrx_app.Adapters.PharmacyAdapter;
import com.myrx_app.ModelClasses.PharmacyModel;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ResultsPharmacyActivity extends AppCompatActivity {
    ResultsPharmacyActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.search)
    Button search;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    private ArrayList<PharmacyModel> list;
    String latitude = "", longitude = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_pharmacy);
        ButterKnife.bind(this);

        context = ResultsPharmacyActivity.this;

        latitude = getIntent().getStringExtra("latitude");
        longitude = getIntent().getStringExtra("longitude");
        list = getIntent().getParcelableArrayListExtra("list");

        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new PharmacyAdapter(context, list));
    }

    private void PHARMACY(String postcode, String latitude, String longitude) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllMobileScriptAPIS.PHARMACY
                + "?postcode=" + postcode + "&latitude=" + latitude + "&longitude=" + longitude, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("data");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                PharmacyModel pharmacyModel = new PharmacyModel();
                                pharmacyModel.setPharmacy_id(object.getString("id"));
                                pharmacyModel.setPharmacy_name(object.getString("name"));
                                pharmacyModel.setAddress(object.optString("address"));
                                pharmacyModel.setDistance(object.getString("total_distance"));
                                pharmacyModel.setLat(object.getString("latitude"));
                                pharmacyModel.setLng(object.getString("longitude"));
                                pharmacyModel.setImage(object.optString("profile"));
                                list.add(pharmacyModel);
                            }

                            if (list.size() > 0) {
                            } else {
                                util.IOSDialog(context, "No Pharmacy Found!");
                            }

                            myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                            myRecyclerView.setAdapter(new PharmacyAdapter(context, list));

                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.back_button, R.id.search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.search:
                if (searchBar.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(this, "Please Enter Zip Code/Name");
                    search.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    if (ConnectivityReceiver.isConnected())
                        PHARMACY(searchBar.getText().toString().trim(), latitude, longitude);
                    else
                        util.IOSDialog(context, util.internet_Connection_Error);

                }
                break;
        }
    }
}