package com.myrx_app.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.myrx_app.ModelClasses.PharmacyModel;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.GPSTracker;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FindPharmacyActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    FindPharmacyActivity context;
    private SavePref savePref;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.search)
    Button search;
    GPSTracker gpsTracker = null;
    private ArrayList<PharmacyModel> list;

    GoogleApiClient googleApiClient;

    Location location = null;
    FusedLocationProviderClient fusedLocationClient = null;

    LocationManager locationManager;

    String latitude = "", longitude = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pharmacy);
        ButterKnife.bind(this);

        context = FindPharmacyActivity.this;
        savePref = new SavePref(context);

        gpsTracker = new GPSTracker(context);
    }

    @OnClick(R.id.search)
    public void onClick() {
        if (searchBar.getText().toString().trim().isEmpty()) {
            util.IOSDialog(this, "Please Enter Zip Code/Name");
            search.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        } else {
            Log.e("locationnnnm", latitude + " : " + longitude);
            if (ConnectivityReceiver.isConnected())
                PHARMACY(searchBar.getText().toString().trim(), latitude, longitude);
            else
                util.IOSDialog(context, util.internet_Connection_Error);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!gpsTracker.canGetLocation()) {
            EnableGPSAutoMatically();
        } else {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
        }
    }

    private void PHARMACY(String postcode, String latitude, String longitude) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllMobileScriptAPIS.PHARMACY
                + "?postcode=" + postcode + "&latitude=" + latitude + "&longitude=" + longitude, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("data");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                PharmacyModel pharmacyModel = new PharmacyModel();
                                pharmacyModel.setPharmacy_id(object.getString("id"));
                                pharmacyModel.setPharmacy_name(object.getString("name"));
                                pharmacyModel.setAddress(object.optString("address"));
                                pharmacyModel.setDistance(object.getString("total_distance"));
                                pharmacyModel.setLat(object.getString("latitude"));
                                pharmacyModel.setLng(object.getString("longitude"));
                                pharmacyModel.setPhone(object.optString("phone"));
                                pharmacyModel.setImage(object.optString("profile"));
                                list.add(pharmacyModel);
                            }
                            if (list.size() > 0) {
                                Intent intent = new Intent(context, ResultsPharmacyActivity.class);
                                intent.putExtra("list", list);
                                intent.putExtra("latitude", latitude);
                                intent.putExtra("longitude", longitude);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, "No Pharmacy Found!");
                            }


                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(FindPharmacyActivity.this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(FindPharmacyActivity.this);
                            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            util.showToast(FindPharmacyActivity.this, "Location Service Turn on Successfully");

                            gpsTracker = new GPSTracker(FindPharmacyActivity.this);


                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(FindPharmacyActivity.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            // util.showToast(SortActivity.this, "Location service turn on RESOLUTION_REQUIRED");

                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            //util.showToast(SortActivity.this, "Location service turn on SETTINGS_CHANGE_UNAVAILABLE");
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                latitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


}