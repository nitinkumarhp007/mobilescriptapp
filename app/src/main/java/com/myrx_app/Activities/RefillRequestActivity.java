package com.myrx_app.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.myrx_app.R;
import com.myrx_app.Util.ConnectivityReceiver;
import com.myrx_app.Util.Parameters;
import com.myrx_app.Util.SavePref;
import com.myrx_app.Util.util;
import com.myrx_app.parser.AllMobileScriptAPIS;
import com.myrx_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RefillRequestActivity extends AppCompatActivity {
    RefillRequestActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.title)
    TextView title;
    private SavePref savePref;
    @BindView(R.id.precription_number)
    EditText precriptionNumber;
    @BindView(R.id.name_of_medication)
    EditText nameOfMedication;
    @BindView(R.id.request)
    Button request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refill_request);
        ButterKnife.bind(this);

        context = RefillRequestActivity.this;
        savePref = new SavePref(context);
        title.setText("Refill Request");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //setToolbar();
    }

   /* private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Refill Request");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }*/

    @OnClick(R.id.request)
    public void onClick() {
        REFILL_REQUESTProcess();
    }

    private void REFILL_REQUESTProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (precriptionNumber.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Prescription Number");
                request.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (nameOfMedication.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name Of Medication");
                request.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                REFILL_REQUEST_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void REFILL_REQUEST_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REQUESTS_MEDISENSE_ID, precriptionNumber.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.MEDISENSE_NAME, nameOfMedication.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllMobileScriptAPIS.REFILL_REQUEST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setMessage("Data Submiited Succesfully").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
